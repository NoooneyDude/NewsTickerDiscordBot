// ticker.js: Entry point for NewsTickerDiscordBot project.
// Regularly updates a channel description with a randomly
// selected news ticker from SimCity 3000.

const fs = require("fs");
const path = require("path");
const Discord = require("discord.js");
const config = require("./config/config.json");
const auth = require("./config/auth.json");
const simTickers = fs.readFileSync("./src/simTickers.txt", "utf8").split("\n");
const otherTickers = fs.readFileSync("./src/otherTickers.txt", "utf8").split("\n");
const newsTickers = simTickers.concat(otherTickers);
const client = new Discord.Client();

client.on("ready", () => {
    console.log("News Ticker started.");
});

// Runs with every seen message.
client.on("message", async message => {
    if (message.author.bot) return;
    if (message.guild === null) {
        message.reply("Commands can only be used inside a guild.");
    }

    if (message.content.indexOf(config.prefix) == 0) { // If (prefix)command
        var args = message.content.slice(config.prefix.length);
    }
    else return;

    args = args.trim().split(/[\n\r\s]+/g);
    let command = args.shift().toLowerCase();

    // Load and run command from file.
    commandPath = "./src/commands/" + path.basename(command) + ".js";
    if (!fs.existsSync(commandPath)) {
        message.reply("the command '" + command + "' doesn't exist. Try `help`.");
        return;
    }

    try {
        let commandFile = require(commandPath);
        commandFile.run(client, message, args, config, newsTickers);
    }
    catch (err) {
        console.error(err);
        message.reply("unable to run the command `" + command + "`.");
    }
});

client.login(auth.token);
