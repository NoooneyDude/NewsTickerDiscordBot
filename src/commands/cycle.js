// cycle.js: Contains module for command 'cycle'.
// Regularly updates the news ticker in the current channel.

exports.help = "`cycle [delay (s)]` - regularly refreshes the channel's news ticker (default 1hr).";

exports.timer = null;

exports.run = (client, message, args, config, newsTickers) => {
    const channel = message.channel;
    const guildName = message.guild.name;
    let interval = 3600;

    if (args.length === 1 && Number.isInteger(parseInt(args[0]))) interval = args[0];
    if (exports.timer !== null) clearInterval(exports.timer);

    message.delete();

    exports.timer = setInterval( function() {
        var randomNumber = Math.floor(Math.random() * newsTickers.length);
        var newsTicker = newsTickers[randomNumber];
        newsTicker = newsTicker.replace("guild name", guildName);
        channel.setTopic(newsTicker);
    }, interval * 1000);
}
