// refresh.js: Contains module for command 'refresh'.
// Forces the bot to refresh the news ticker for the current channel.

exports.help = "`refresh` - refreshes the channel's news ticker.";

exports.run = (client, message, args, config, newsTickers) => {
    const randomNumber = Math.floor(Math.random() * newsTickers.length);
    let newsTicker = newsTickers.splice(randomNumber, 1)[0];
    newsTicker = newsTicker.replace("guild name", message.guild.name);
    message.channel.setTopic(newsTicker);
    message.delete();
}