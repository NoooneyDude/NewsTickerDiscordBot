// help.js: Contains module for command 'help'.
// Sends the summoner a direct message with a list of available commands.

const fs = require("fs");

exports.help = "`help` - displays this help message.";

exports.run = (client, message, args, config, newsTickers) => {
    fs.readdir("./src/commands/", (err, files) => {
        let helpLine = 
            "__**News Ticker Help**__"
            + "\nTo execute a command, you must prefix commands with either " + client.user + ", or `" + config.prefix + "`."
            + "\n`<>` arguments are required, while `[]` arguments are optional.\n\n";

        if (files.length == 0) {
            message.channel.send("**There are no available commands.**");
            return;
        }

        helpLine += "**Available " + (files.length == 1 ? "command" : "commands") + ":**\n";

        files.forEach(file => {
            try {
                let command = require("./" + file);
                helpLine += command.help + "\n";
            }
            catch (err) {
                console.error(err);
            }
        });

        helpLine += "\nYou can check out the bot's `GitLab Repo` here: <https://gitlab.com/NoooneyDude/NewsTickerDiscordBot>.";

        message.reply("you've been sent a direct message.");
        message.author.send(helpLine);
    });
}